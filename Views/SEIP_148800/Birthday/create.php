<?php
require_once ("../../../vendor/autoload.php");
use App\Message\Message;
echo message ::getMessage();




?>





<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login Form Template</title>

    <!-- CSS -->

    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
     <![endif]-->

    <!-- Favicon and touch icons -->


</head>

<body>

<!-- Top content -->
<div  class="top-content" >

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>Bootstrap</strong> Login Form</h1>
                    <div class="description">
                        <p>
                            This is a free responsive login form made with Bootstrap.
                            Download it on <a href="http://azmind.com"><strong>AZMIND</strong></a>, customize and use it as you like!
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Book Title</h3>
                            <p>Enter your BookTitle and Author name to log on!</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-book"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Book Title</label>
                                <input type="text" name="book_title" placeholder="Book_Title" class="form-username form-control" id="form-username">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">Author Namae</label>
                                <input type="text" name="author_name" placeholder="Author_name" class="form-password form-control" id="form-password">
                            </div>
                            <button type="submit" class="btn">Sign in!</button>
                        </form>
                    </div>
                </div>
            </div>

                </div>
            </div>
        </div>
    </div>

</div>


<!-- Javascript -->
<script src="../../../resourse/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resourse/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resourse/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resourse/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resourse/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>